package com.example.joyce.views2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SimpleAdapter extends BaseAdapter{

    String []lanches;
    Context context;
    static LayoutInflater inflater = null;

    public SimpleAdapter(String[] lanches, Context context){
        this.lanches=lanches;
        this.context = context;
    }

    @Override
    public int getCount() {
        return lanches.length;
    }

    @Override
    public Object getItem(int position) {
        return getItemId(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //LayoutInflater inflater = LayoutInflater.from(context);
        //View layout = inflater.inflate(R.layout.activity_simple_adapter, parent, false);

        View row = convertView;
        if (row == null){
            inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.activity_simple_adapter, null);
        }

        TextView txt = convertView.findViewById(R.id.txtLanche);
        txt.setText(lanches[position]);
        return row;
    }
}
