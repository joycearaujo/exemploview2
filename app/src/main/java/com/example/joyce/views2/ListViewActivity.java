package com.example.joyce.views2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ListViewActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        List <String> comida = new ArrayList<>();
        comida.add("coxinha");
        comida.add("pizza");
        comida.add("Torta de frango");

        ListView listaComida = (ListView) findViewById(R.id.lista_comida);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, comida);

        listaComida.setAdapter(arrayAdapter);

    }
}
