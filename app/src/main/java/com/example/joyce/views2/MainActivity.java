package com.example.joyce.views2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void ProgressActivity(View view) {
        Intent intent = new Intent(getApplicationContext(), ProgressDialogActivity.class);
        startActivity(intent);
    }

    public void BarActivity(View view) {
        Intent intent = new Intent(getApplicationContext(), ProgressBarActivity.class);
        startActivity(intent);
    }

    public void alertActivity(View view) {
        Intent intent = new Intent(getApplicationContext(), AlertDialogActivity.class);
        startActivity(intent);
    }

    public void listaActivity(View view) {
        Intent intent = new Intent(getApplicationContext(), ListViewActivity.class);
        startActivity(intent);
    }

    public void adapterActivity(View view) {
        Intent intent = new Intent(getApplicationContext(), SimpleAdapaterActivity.class);
        startActivity(intent);
    }
}
