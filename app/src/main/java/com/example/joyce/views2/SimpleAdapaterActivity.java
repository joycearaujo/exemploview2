package com.example.joyce.views2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class SimpleAdapaterActivity extends AppCompatActivity {
    private ListView lcomida;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_adapter);

        String[] lanches ={"Pavê"};

        lcomida = (ListView) findViewById(R.id.lista_comida);
        SimpleAdapter simpleAdapter = new SimpleAdapter(lanches, SimpleAdapaterActivity.this);
        lcomida.setAdapter(simpleAdapter);
    }
}
