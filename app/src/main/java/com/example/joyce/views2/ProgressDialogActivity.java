package com.example.joyce.views2;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class ProgressDialogActivity extends AppCompatActivity {

    ProgressDialog pd;
    Button buttonpd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progres_dialog);


    }

    public void Progress(View view) {
        pd = ProgressDialog.show(ProgressDialogActivity.this, "Exemplo View pt2", "Carregamento...");
        pd.setCancelable(false);

        new Thread(){
            @Override
            public void run() {
                super.run();
                try {
                    sleep(5000);
                    pd.dismiss();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }
}
