package com.example.joyce.views2;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import static java.lang.Thread.sleep;

public class ProgressBarActivity extends AppCompatActivity {
    private Handler handler = new Handler();
    int status=0;
    ProgressBar pbh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress_bar);

        pbh = (ProgressBar) findViewById(R.id.progressBar);

        new Thread(new Runnable(){
          public void run(){
              while (status<100){
                  status +=1;
                  handler.post(new Runnable() {
                      @Override
                      public void run() {
                          pbh.setProgress(status);
                      }
                  });
                  try {
                      sleep(200);
                  }catch (InterruptedException e) {
                      e.printStackTrace();
                  }
              }
          }
        }).start();

    }

}
